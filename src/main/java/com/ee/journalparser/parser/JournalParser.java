package com.ee.journalparser.parser;

import com.ee.journalparser.parser.objects.*;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class JournalParser {

    public static List<JournalEntryDTO> parse(String path) {
        List<JournalEntryDTO> journal = new ArrayList<>();

        JournalEntryDTO entry = null;
        ClassificationDTO classification = null;
        CaseFileDTO caseFile = null;
        RecordDTO record = null;
        List<PartyDTO> parties = null;
        PartyDTO party = null;

        boolean buildingClass = false;
        boolean buildingCaseFile = false;
        boolean buildingRecord = false;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(path));
            while (reader.hasNext()) {
                XMLEvent nextEvent = reader.nextEvent();
                if (nextEvent.isStartElement()) {
                    StartElement startElement = nextEvent.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case "entry":
                            entry = new JournalEntryDTO();
                            break;
                        case "class":
                            buildingClass = true;
                            classification = new ClassificationDTO();
                            break;
                        case "caseFile":
                            buildingCaseFile = true;
                            caseFile = new CaseFileDTO();
                            break;
                        case "record":
                            buildingRecord = true;
                            record = new RecordDTO();
                            break;
                        case "party":
                            party = new PartyDTO();
                            break;
                        case "classIdent":
                            nextEvent = reader.nextEvent();
                            classification.setClassIdent(nextEvent.asCharacters().getData());
                            break;
                        case "title":
                            nextEvent = reader.nextEvent();
                            if (buildingClass) {
                                classification.setTitle(nextEvent.asCharacters().getData());
                            }
                            if (buildingCaseFile) {
                                caseFile.setTitle(nextEvent.asCharacters().getData());
                            }
                            if (buildingRecord) {
                                record.setTitle(nextEvent.asCharacters().getData());
                            }
                            break;
                        case "caseYear":
                            nextEvent = reader.nextEvent();
                            caseFile.setCaseYear(Integer.parseInt(nextEvent.asCharacters().getData()));
                            break;
                        case "caseSequenceNumber":
                            nextEvent = reader.nextEvent();
                            caseFile.setCaseSequenceNumber(Integer.parseInt(nextEvent.asCharacters().getData()));
                            break;
                        case "publicTitle":
                            nextEvent = reader.nextEvent();
                            if (buildingCaseFile && nextEvent.isCharacters()) {
                                caseFile.setPublicTitle(nextEvent.asCharacters().getData());
                            }
                            if (buildingRecord && nextEvent.isCharacters()) {
                                record.setPublicTitle(nextEvent.asCharacters().getData());
                            }
                            break;
                        case "recordYear":
                            nextEvent = reader.nextEvent();
                            record.setRecordYear(Integer.parseInt(nextEvent.asCharacters().getData()));
                            break;
                        case "recordSequenceNumber":
                            nextEvent = reader.nextEvent();
                            record.setRecordSequenceNumber(Integer.parseInt(nextEvent.asCharacters().getData()));
                            break;
                        case "recordNumber":
                            nextEvent = reader.nextEvent();
                            record.setRecordNumber(Integer.parseInt(nextEvent.asCharacters().getData()));
                            break;
                        case "recordDate":
                            nextEvent = reader.nextEvent();
                            record.setRecordDate(nextEvent.asCharacters().getData());
                            break;
                        case "parties":
                            parties = new ArrayList<>();
                            break;
                        case "partyType":
                            nextEvent = reader.nextEvent();
                            party.setPartyType(nextEvent.asCharacters().getData());
                            break;
                        case "partyName":
                            nextEvent = reader.nextEvent();
                            party.setPartyName(nextEvent.asCharacters().getData());
                            break;
                    }
                }

                if (nextEvent.isEndElement()) {
                    EndElement endElement = nextEvent.asEndElement();
                    switch (endElement.getName().getLocalPart()) {
                        case "party":
                            parties.add(party);
                            break;
                        case "record":
                            record.setParties(parties);
                            entry.setRecord(record);
                            buildingRecord = false;
                            break;
                        case "class":
                            entry.setClassification(classification);
                            buildingClass = false;
                            break;
                        case "caseFile":
                            entry.setCaseFile(caseFile);
                            buildingCaseFile = false;
                            break;
                        case "entry":
                            journal.add(entry);
                            break;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

        return journal;
    }
}
