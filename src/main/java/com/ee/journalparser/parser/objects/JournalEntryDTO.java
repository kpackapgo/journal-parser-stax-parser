package com.ee.journalparser.parser.objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class JournalEntryDTO implements Serializable {

    @JsonProperty("class")
    private ClassificationDTO classification;
    private CaseFileDTO caseFile;
    private RecordDTO record;
}
