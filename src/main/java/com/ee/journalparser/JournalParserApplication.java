package com.ee.journalparser;

import com.ee.journalparser.parser.objects.JournalEntryDTO;
import com.ee.journalparser.parser.JournalParser;
import com.ee.journalparser.parser.JournalValidator;
import com.ee.journalparser.upload.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableAsync;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@EnableAsync
@SpringBootApplication
public class JournalParserApplication implements CommandLineRunner {

	@Autowired
	UploadService uploadService;

	public static void main(String[] args) {
		SpringApplication.run(JournalParserApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		//Getting the parameters from the user input
		String  xmlFileNameWithExtension = args[0];
		String schemaFileNameWithExtension = args[1];
		File schemaFile = new File(schemaFileNameWithExtension);

		//Validating the xml file
		try {
			JournalValidator.validate(new FileInputStream(xmlFileNameWithExtension), schemaFile.toURI().toURL());
		} catch (SAXException e) {
			System.out.println("Your file is not valid. Please fix error:" + e.getMessage());
			System.exit(0);
		}

		//Parsing the xml file
		List<JournalEntryDTO> parse = JournalParser.parse(xmlFileNameWithExtension);

		List<CompletableFuture<HttpStatus>> allFutures = new ArrayList<>();

		for (JournalEntryDTO journalEntryDTO : parse) {
			allFutures.add(uploadService.upload(journalEntryDTO));
		}

		//Send entries on the server to bes stored
		CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[0])).join();

		System.out.println("FILE UPLOAD FINISHED. SAVED: " + parse.size() +" ENTRIES.");
		System.exit(0);
	}
}
