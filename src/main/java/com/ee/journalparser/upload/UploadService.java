package com.ee.journalparser.upload;

import com.ee.journalparser.parser.objects.JournalEntryDTO;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.CompletableFuture;

public interface UploadService {

    @Async
    CompletableFuture<HttpStatus> upload(JournalEntryDTO entry);

}
