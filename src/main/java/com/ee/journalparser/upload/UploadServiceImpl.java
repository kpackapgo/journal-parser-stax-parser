package com.ee.journalparser.upload;

import com.ee.journalparser.parser.objects.JournalEntryDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class UploadServiceImpl implements UploadService {

    @Autowired
    private RestTemplate restTemplate;

    @Async
    public CompletableFuture<HttpStatus> upload(JournalEntryDTO entry) {

        String apiUrl = "http://localhost:8080/api/entry";
        HttpEntity<JournalEntryDTO> request = new HttpEntity<>(entry);
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";
        try {
            jsonString = mapper.writeValueAsString(entry);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestInJson = new HttpEntity<>(jsonString, headers);
        ResponseEntity<Void> responseObj = restTemplate.exchange(apiUrl, HttpMethod.POST, requestInJson, Void.class);
        return CompletableFuture.completedFuture(responseObj.getStatusCode());
    }
}
